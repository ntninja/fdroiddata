Repo Type:git
Repo:https://github.com/curl/curl.git

# Run autotools stuff to generate the `./configure` script that will make the checkout look like
# a source release of libcurl to consumers
Prepare:./buildconf
